local T, C, L = Tukui:unpack()

local ClassTimer = T["ClassTimer"]

ClassTimer.Filter["TRINKET"] = {
	ClassTimer:CreateSpellEntry("Meticulous Scheming"),
	ClassTimer:CreateSpellEntry("Seize the Moment!"),
};
